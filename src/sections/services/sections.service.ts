import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { throwErrorIfNull } from 'src/shared/utils/error-helpers';
import { SUCH_IDENTIFIER_DOES_NOT_EXIST } from '../../shared/constants/error-messages';
import { Section } from '../models/section.model';
import { SectionDocument } from '../schemas/section.schema';

@Injectable()
export class SectionsService {
    constructor(
        @InjectModel(Section.name)
        private sectionModel: Model<SectionDocument>
    ) {}

    async create(section: Section): Promise<Section> {
        const alreadyExists = await this.sectionModel
            .findOne({
                name: section.name
            })
            .lean();

        if (alreadyExists) {
            throw new Error('Section with such a name already exists');
        }

        return (await new this.sectionModel(section).save()).toObject();
    }

    async getAll(): Promise<Section[]> {
        return await this.sectionModel.find().lean();
    }

    getById(id: string): Promise<Section> {
        return this.sectionModel
            .findById(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    update(id: string, newSection: Partial<Section>): Promise<Section> {
        return this.sectionModel
            .findByIdAndUpdate(id, newSection, {
                new: true
            })
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    delete(id: string): Promise<Section> {
        return this.sectionModel
            .findByIdAndDelete(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }
}
