import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SectionsController } from './controllers/sections.controller';
import { Section } from './models/section.model';
import { SectionSchema } from './schemas/section.schema';
import { SectionsService } from './services/sections.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: Section.name, schema: SectionSchema }
        ])
    ],
    controllers: [SectionsController],
    providers: [SectionsService]
})
export class SectionsModule {}
