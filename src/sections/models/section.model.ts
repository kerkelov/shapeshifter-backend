import { IsMongoId } from 'class-validator';
import { IsNameWithReasonableLength } from 'src/shared/decorators/is-name-with-reasonable-length.decorator';

export class Section {
    @IsNameWithReasonableLength()
    name: string;

    @IsMongoId({ each: true })
    workoutTemplates: string[];

    @IsMongoId()
    user: string;
}
