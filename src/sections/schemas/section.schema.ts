import { Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Required } from 'src/shared/utils/mongoose-helpers';
import { User } from 'src/users/models/user.model';
import { WorkoutTemplate } from 'src/workout-templates/models/workout-template.model';
import { RequiredWithType } from '../../shared/utils/mongoose-helpers';

@Schema()
export class SectionSchemaTemplate {
    @Required()
    name: string;

    @RequiredWithType([
        { type: mongoose.Types.ObjectId, ref: WorkoutTemplate.name }
    ])
    workoutTemplates: string[];

    @RequiredWithType(mongoose.Types.ObjectId, { ref: User.name })
    user: string;
}

export type SectionDocument = SectionSchemaTemplate & Document;

export const SectionSchema = SchemaFactory.createForClass(
    SectionSchemaTemplate
);
