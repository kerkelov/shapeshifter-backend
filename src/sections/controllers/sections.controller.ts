import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post
} from '@nestjs/common';
import { AllowForRoles } from 'src/auth/decorators/allow-for-roles.decorator';
import { Section } from 'src/sections/models/section.model';
import { SectionsService } from 'src/sections/services/sections.service';
import { Role } from 'src/shared/enums/role';
import { wrapInHttpExceptionAndRethrow } from 'src/shared/utils/error-helpers';
import { HasIdentifier } from '../../shared/models/has-identifier.model';

@Controller('sections')
@AllowForRoles(Role.User)
export class SectionsController {
    constructor(private sectionsService: SectionsService) {}

    @Post()
    @HttpCode(HttpStatus.CREATED)
    create(@Body() section: Section): Promise<Section> {
        return this.sectionsService
            .create(section)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get()
    getAll(): Promise<Section[]> {
        return this.sectionsService
            .getAll()
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get(':id')
    getById(@Param() params: HasIdentifier): Promise<Section> {
        return this.sectionsService
            .getById(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Patch(':id')
    update(
        @Param() params: HasIdentifier,
        @Body() partsToUpdate: Partial<Section>
    ): Promise<Section> {
        return this.sectionsService
            .update(params.id, partsToUpdate)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    delete(@Param() params: HasIdentifier): Promise<Section> {
        return this.sectionsService
            .delete(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }
}
