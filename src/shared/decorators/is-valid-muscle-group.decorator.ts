import { BadRequestException } from '@nestjs/common';
import { registerDecorator } from 'class-validator';
import {
    MuscleGroup,
    validMuscleGroups
} from '../../exercises/models/muscle-groups.model';

type IsValidMuscleGroupOptions = { each?: boolean };
const validMucleGroupsSet = new Set(validMuscleGroups);

export function IsValidMuscleGroup(
    options?: IsValidMuscleGroupOptions
): (object: any, propertyName: string) => void {
    return function (object: any, propertyName: string) {
        registerDecorator({
            name: 'IsValidMuscleGroup',
            target: object.constructor,
            propertyName: propertyName,
            validator: getValidator(propertyName, options)
        });
    };
}

function getValidator(
    propertyName: string,
    options?: IsValidMuscleGroupOptions
): {
    validate(muscleGroups: string | string[]): boolean;
} {
    return {
        validate(muscleGroups: string | string[]) {
            if (Array.isArray(muscleGroups) && options?.each) {
                muscleGroups.forEach((muscleGroup) => {
                    if (!validMucleGroupsSet.has(muscleGroup as MuscleGroup)) {
                        throw new BadRequestException(
                            `In ${propertyName}, ${muscleGroup} is not valid muscle group`
                        );
                    }
                });

                return true;
            }

            if (!validMucleGroupsSet.has(muscleGroups as MuscleGroup)) {
                throw new BadRequestException(
                    `${propertyName} is not valid muscle group`
                );
            }

            return true;
        }
    };
}
