import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { User } from '../../users/models/user.model';

export const CurrentUser = createParamDecorator(
    (desiredProperty: string, ctx: ExecutionContext) => {
        const request = ctx.switchToHttp().getRequest(),
            user: User = request.user;

        return desiredProperty ? (user as any)?.[desiredProperty] : user;
    }
);
