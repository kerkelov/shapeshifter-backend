import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory, Reflector } from '@nestjs/core';
import MongoStore from 'connect-mongo';
import session from 'express-session';
import passport from 'passport';
import { AppModule } from './app.module';
import { RolesGuard } from './shared/guards/roles.guard';

async function bootstrap() {
    const app = await NestFactory.create(AppModule),
        config = app.get(ConfigService),
        reflector = app.get(Reflector),
        logger = new Logger('Main'),
        sessionStore = MongoStore.create({
            mongoUrl: config.get('CONNECTION_STRING')
        });

    app.enableCors({ credentials: true, origin: 'http://localhost:4200' });

    //TODO: understand why turning on the whitelist option is causing bugs with the exercise executions, not populating the set property correctly
    app.useGlobalPipes(new ValidationPipe({ transform: true }))
        .useGlobalGuards(new RolesGuard(reflector))
        .use(
            session({
                store: sessionStore,
                secret: config.get('SESSION_SECRET') || 'supersecretstring',
                resave: false,
                saveUninitialized: false,
                cookie: { maxAge: 7 * 24 * 60 * 60 * 1000 } // 7 days
            })
        )
        .use(passport.initialize())
        .use(passport.session());

    await app.listen(config.get('PORT') || 3000, () =>
        logger.log(`listening on port: ${config.get('PORT')}`)
    );
}

bootstrap();
