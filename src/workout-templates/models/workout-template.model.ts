import { IsMongoId } from 'class-validator';
import { IsNameWithReasonableLength } from 'src/shared/decorators/is-name-with-reasonable-length.decorator';

export class WorkoutTemplate {
    @IsNameWithReasonableLength()
    name: string;

    @IsMongoId({ each: true })
    exercises: string[];
}
