import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post
} from '@nestjs/common';
import { AllowForRoles } from 'src/auth/decorators/allow-for-roles.decorator';
import { Role } from 'src/shared/enums/role';
import { wrapInHttpExceptionAndRethrow } from 'src/shared/utils/error-helpers';
import { HasIdentifier } from '../../shared/models/has-identifier.model';
import { WorkoutTemplate } from '../models/workout-template.model';
import { WorkoutTemplatesService } from '../services/workout-templates.service';

@Controller('workout-templates')
@AllowForRoles(Role.User)
export class WorkoutTemplatesController {
    constructor(private workoutTemplatesService: WorkoutTemplatesService) {}

    @Post()
    @HttpCode(HttpStatus.CREATED)
    create(@Body() workoutTemplate: WorkoutTemplate): Promise<WorkoutTemplate> {
        return this.workoutTemplatesService
            .create(workoutTemplate)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get()
    getAll(): Promise<WorkoutTemplate[]> {
        return this.workoutTemplatesService
            .getAll()
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get(':id')
    getById(@Param() params: HasIdentifier): Promise<WorkoutTemplate> {
        return this.workoutTemplatesService
            .getById(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Patch(':id')
    update(
        @Param() params: HasIdentifier,
        @Body() partsToUpdate: Partial<WorkoutTemplate>
    ): Promise<WorkoutTemplate> {
        return this.workoutTemplatesService
            .update(params.id, partsToUpdate)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    delete(@Param() params: HasIdentifier): Promise<WorkoutTemplate> {
        return this.workoutTemplatesService
            .delete(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }
}
