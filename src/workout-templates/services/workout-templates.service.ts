import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SUCH_IDENTIFIER_DOES_NOT_EXIST } from 'src/shared/constants/error-messages';
import { throwErrorIfNull } from 'src/shared/utils/error-helpers';
import { WorkoutTemplate } from '../models/workout-template.model';
import { WorkoutTemplateDocument } from '../schemas/workout-template.schema';

@Injectable()
export class WorkoutTemplatesService {
    constructor(
        @InjectModel(WorkoutTemplate.name)
        private workoutTemplateModel: Model<WorkoutTemplateDocument>
    ) {}

    async create(workoutTemplate: WorkoutTemplate): Promise<WorkoutTemplate> {
        const alreadyExists = await this.workoutTemplateModel
            .findOne({
                name: workoutTemplate.name
            })
            .lean();

        if (alreadyExists) {
            throw new Error('Workout template with such a name already exists');
        }

        return (
            await new this.workoutTemplateModel(workoutTemplate).save()
        ).toObject();
    }

    async getAll(): Promise<WorkoutTemplate[]> {
        return await this.workoutTemplateModel.find().lean();
    }

    getById(id: string): Promise<WorkoutTemplate> {
        return this.workoutTemplateModel
            .findById(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    update(
        id: string,
        partsToUpdate: Partial<WorkoutTemplate>
    ): Promise<WorkoutTemplate> {
        return this.workoutTemplateModel
            .findByIdAndUpdate(id, partsToUpdate, {
                new: true
            })
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    delete(id: string): Promise<WorkoutTemplate> {
        return this.workoutTemplateModel
            .findByIdAndDelete(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }
}
