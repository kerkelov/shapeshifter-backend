import { Test, TestingModule } from '@nestjs/testing';
import { WorkoutTemplatesService } from './workout-templates.service';

describe('WorkoutTemplatesService', () => {
    let service: WorkoutTemplatesService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [WorkoutTemplatesService]
        }).compile();

        service = module.get<WorkoutTemplatesService>(WorkoutTemplatesService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
