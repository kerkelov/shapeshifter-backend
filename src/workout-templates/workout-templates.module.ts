import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WorkoutTemplatesController } from './controllers/workout-templates.controller';
import { WorkoutTemplate } from './models/workout-template.model';
import { WorkoutTemplateSchema } from './schemas/workout-template.schema';
import { WorkoutTemplatesService } from './services/workout-templates.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: WorkoutTemplate.name, schema: WorkoutTemplateSchema }
        ])
    ],
    controllers: [WorkoutTemplatesController],
    providers: [WorkoutTemplatesService]
})
export class WorkoutTemplatesModule {}
