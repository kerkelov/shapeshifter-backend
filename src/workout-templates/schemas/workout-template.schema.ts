import { Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Exercise } from 'src/exercises/models/exercise.model';
import { Required, RequiredWithType } from 'src/shared/utils/mongoose-helpers';

@Schema()
export class WorkoutTemplateSchemaTemplate {
    @Required()
    name: string;

    @RequiredWithType([{ type: mongoose.Types.ObjectId, ref: Exercise.name }])
    exercises: string[];
}

export type WorkoutTemplateDocument = WorkoutTemplateSchemaTemplate & Document;

export const WorkoutTemplateSchema = SchemaFactory.createForClass(
    WorkoutTemplateSchemaTemplate
);
