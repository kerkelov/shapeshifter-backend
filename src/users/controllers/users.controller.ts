import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post
} from '@nestjs/common';
import { AllowForRoles } from 'src/auth/decorators/allow-for-roles.decorator';
import { wrapInHttpExceptionAndRethrow } from 'src/shared/utils/error-helpers';
import { User } from 'src/users/models/user.model';
import { UsersService } from 'src/users/services/users.service';
import { CurrentUser } from '../../shared/decorators/current-user.decorator';
import { Role } from '../../shared/enums/role';
import { HasIdentifier } from '../../shared/models/has-identifier.model';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) {}

    @Post()
    @HttpCode(HttpStatus.CREATED)
    create(@Body() user: User): Promise<User> {
        user.roles = [Role.User];

        return this.usersService
            .create(user)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get()
    getAll(): Promise<User[]> {
        return this.usersService.getAll().catch(wrapInHttpExceptionAndRethrow);
    }

    @Get(':id')
    getById(@Param() params: HasIdentifier): Promise<User> {
        return this.usersService
            .getById(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Patch(':id')
    @AllowForRoles(Role.User)
    update(
        @Param() params: HasIdentifier,
        @CurrentUser() user: User,
        @Body('partsToUpdate') partsToUpdate: Partial<User>,
        @Body('password') password: string
    ): Promise<User> {
        return this.usersService
            .update(
                params.id,
                partsToUpdate,
                user?.roles.includes(Role.Admin),
                password
            )
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    @AllowForRoles(Role.User)
    delete(
        @Param() params: HasIdentifier,
        @CurrentUser() user: User,
        @Body('password') password: string
    ): Promise<User> {
        return this.usersService
            .delete(params.id, user?.roles.includes(Role.Admin), password)
            .catch(wrapInHttpExceptionAndRethrow);
    }
}
