import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersController } from './controllers/users.controller';
import { UserSchema } from './schemas/user.schema';
import { UsersService } from './services/users.service';
import { PasswordsService } from './services/passwords.service';
import { User } from './models/user.model';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: User.name, schema: UserSchema }])
    ],
    controllers: [UsersController],
    providers: [UsersService, PasswordsService],
    exports: [UsersService, PasswordsService]
})
export class UsersModule {}
