import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SUCH_IDENTIFIER_DOES_NOT_EXIST } from 'src/shared/constants/error-messages';
import { UserDocument } from 'src/users/schemas/user.schema';
import { throwErrorIfNull } from '../../shared/utils/error-helpers';
import { User } from '../models/user.model';
import { PasswordsService } from './passwords.service';
2;
const withoutPasswordProjection = { password: 0 };

@Injectable()
export class UsersService {
    constructor(
        @InjectModel(User.name)
        private userModel: Model<UserDocument>,
        private passwordsService: PasswordsService
    ) {}

    async create(user: User): Promise<User> {
        const withTheSameEmail = {
                email: user.email
            },
            isAlreadyCreated = await this.userModel
                .findOne(withTheSameEmail)
                .lean();

        if (isAlreadyCreated) {
            throw new Error('User with such email already exists');
        }

        user.password = await this.passwordsService.hash(user.password);

        await new this.userModel(user).save();

        return await this.userModel
            .findOne(withTheSameEmail, withoutPasswordProjection)
            .lean();
    }

    async getAll(): Promise<User[]> {
        return await this.userModel.find({}, withoutPasswordProjection).lean();
    }

    getById(id: string): Promise<User> {
        return this.userModel
            .findById(id, withoutPasswordProjection)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    getByEmail(email: string): Promise<User> {
        return this.userModel
            .findOne({ email }, withoutPasswordProjection)
            .lean()
            .then(throwErrorIfNull('User with such an email does not exist'));
    }

    getNotSerialized(params: { id?: string; email?: string }): Promise<User> {
        const filter: { _id?: string; email?: string } = {};
        if (params.id) {
            filter._id = params.id;
        } else if (params.email) {
            filter.email = params.email;
        } else {
            throw new Error('Either id or email should be provided');
        }

        return this.userModel
            .findOne(filter)
            .lean()
            .then(
                throwErrorIfNull('Record with this identifier does not exist')
            );
    }

    async update(
        id: string,
        partsToUpdate: Partial<User>,
        isAdmin: boolean,
        password: string
    ): Promise<User> {
        const isUpdatingCredentials =
            partsToUpdate?.email || partsToUpdate?.password;

        if (isUpdatingCredentials && !isAdmin) {
            const arePasswordsMatching = await this.belongsToUserWithId(
                id,
                password
            );

            if (!arePasswordsMatching) {
                throw new Error('You are not authorized to make this change');
            }
        }

        if (partsToUpdate?.password) {
            partsToUpdate.password = await this.passwordsService.hash(
                partsToUpdate.password
            );
        }

        if (partsToUpdate?.email) {
            const userWithTheSameEmail = await this.userModel
                .findOne({ email: partsToUpdate.email })
                .lean();

            if (userWithTheSameEmail) {
                throw new Error('User with that email already exists!');
            }
        }

        return this.userModel
            .findOneAndUpdate({ _id: id }, partsToUpdate, {
                new: true,
                projection: withoutPasswordProjection
            })
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    async delete(
        id: string,
        isAdmin: boolean,
        password: string
    ): Promise<User> {
        if (!isAdmin) {
            const arePasswordsMatching = await this.belongsToUserWithId(
                id,
                password
            );

            if (!arePasswordsMatching) {
                throw new Error('You are not authorized to make that change!');
            }
        }

        return this.userModel
            .findByIdAndDelete(id, { projection: withoutPasswordProjection })
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    private async belongsToUserWithId(
        id: string,
        password: string
    ): Promise<boolean> {
        const correctPasswordHash = (await this.getNotSerialized({ id }))
            .password;

        const arePasswordsMatching = await this.passwordsService.compare(
            password,
            correctPasswordHash
        );

        return arePasswordsMatching;
    }
}
