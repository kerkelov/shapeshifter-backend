import { Injectable } from '@nestjs/common';
import argon from 'argon2';

@Injectable()
export class PasswordsService {
    hash(passwordText: string): Promise<string> {
        return argon.hash(passwordText);
    }

    compare(
        passwordAsPlainText: string,
        passwordHash: string
    ): Promise<boolean> {
        if (!passwordAsPlainText || !passwordHash) {
            return Promise.resolve(false);
        }

        return argon.verify(passwordHash, passwordAsPlainText);
    }
}
