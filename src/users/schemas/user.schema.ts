import { Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Gender, Role } from 'src/shared/enums';
import { Required, RequiredWithType } from 'src/shared/utils/mongoose-helpers';

@Schema()
export class UserSchemaTemplate {
    @Required()
    profilePictureUrl: string;

    @Required()
    username: string;

    @Required()
    email: string;

    @Required()
    password: string;

    @RequiredWithType([String], { enum: ['user', 'admin'] })
    roles: Role[];

    @RequiredWithType(String, { enum: ['male', 'female'] })
    gender: Gender;

    @Required()
    dateOfBirth: Date;

    @Required()
    height: number;
}

export type UserDocument = UserSchemaTemplate & Document;

export const UserSchema = SchemaFactory.createForClass(UserSchemaTemplate);
