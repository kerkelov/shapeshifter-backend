import { IsEmail, IsIn, IsUrl, Length } from 'class-validator';
import { IsNameWithReasonableLength } from 'src/shared/decorators/is-name-with-reasonable-length.decorator';
import { IsNumberInRange } from 'src/shared/decorators/is-number-in-range.decorator';
import { Gender, Role } from 'src/shared/enums';
import { IsDateInReasonableRange } from '../../shared/decorators/is-date-in-reasonable-range.decorator';

export class User {
    @IsUrl()
    profilePictureUrl: string;

    @IsNameWithReasonableLength()
    username: string;

    @IsEmail()
    @Length(8, 50)
    email: string;

    @Length(6, 30)
    password: string;

    roles: Role[];

    @IsIn(['male', 'female'])
    gender: Gender;

    @IsDateInReasonableRange()
    dateOfBirth: Date;

    @IsNumberInRange(100, 230)
    height: number;
}
