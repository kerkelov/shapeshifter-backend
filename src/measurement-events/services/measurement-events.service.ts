import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SUCH_IDENTIFIER_DOES_NOT_EXIST } from 'src/shared/constants/error-messages';
import { throwErrorIfNull } from 'src/shared/utils/error-helpers';
import { MeasurementEvent } from '../models/measurement-event.model';
import { MeasurementEventDocument } from '../schemas/measurement-event.schema';

@Injectable()
export class MeasurementEventsService {
    constructor(
        @InjectModel(MeasurementEvent.name)
        private measurementEventModel: Model<MeasurementEventDocument>
    ) {}

    async create(
        measurementEvent: MeasurementEvent
    ): Promise<MeasurementEvent> {
        const alreadyExists = await this.measurementEventModel
            .findOne({
                date: measurementEvent.date
            })
            .lean();

        if (alreadyExists) {
            throw new Error('Measurement event for that day already submitted');
        }

        return (
            await new this.measurementEventModel(measurementEvent).save()
        ).toObject();
    }

    async getAll(): Promise<MeasurementEvent[]> {
        return await this.measurementEventModel.find().lean();
    }

    getById(id: string): Promise<MeasurementEvent> {
        return this.measurementEventModel
            .findById(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    getByDate(date: Date, userId: string): Promise<MeasurementEvent> {
        const withTheSameDateAndUser = {
            date,
            user: userId
        };

        return this.measurementEventModel
            .findOne(withTheSameDateAndUser)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    async getBetweenDates(
        startDate: Date,
        endDate: Date,
        userId: string
    ): Promise<MeasurementEvent[]> {
        const withDateInTheGivenRangeAndTheSameUser = {
            date: { $gte: startDate, $lte: endDate },
            user: userId
        };

        return await this.measurementEventModel
            .find(withDateInTheGivenRangeAndTheSameUser)
            .lean();
    }

    update(
        id: string,
        newMeasurementEvent: Partial<MeasurementEvent>
    ): Promise<MeasurementEvent> {
        return this.measurementEventModel
            .findByIdAndUpdate(id, newMeasurementEvent, {
                new: true
            })
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    delete(id: string): Promise<MeasurementEvent> {
        return this.measurementEventModel
            .findByIdAndDelete(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }
}
