import { IsMongoId } from 'class-validator';
import { IsArrayOfUrls } from 'src/shared/decorators/is-array-of-urls.decorator';
import { IsDateInReasonableRange } from 'src/shared/decorators/is-date-in-reasonable-range.decorator';
import { IsNumberInRange } from 'src/shared/decorators/is-number-in-range.decorator';

export class MeasurementEvent {
    @IsArrayOfUrls({ minAllowedLength: 1, maxAllowedLength: 5 })
    photosUrls: string[];

    @IsNumberInRange(10, 250)
    weight: number; // in kilograms

    @IsNumberInRange(20, 150)
    chest: number;

    @IsNumberInRange(20, 150)
    waist: number;

    @IsNumberInRange(20, 150)
    hips: number;

    @IsNumberInRange(5, 150)
    biceps: number;

    @IsDateInReasonableRange()
    date: Date;

    @IsMongoId()
    user: string;
}
