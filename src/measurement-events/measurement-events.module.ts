import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MeasurementEventsController } from './controllers/measurement-events.controller';
import { MeasurementEventSchema } from './schemas/measurement-event.schema';
import { MeasurementEventsService } from './services/measurement-events.service';
import { MeasurementEvent } from './models/measurement-event.model';
@Module({
    imports: [
        MongooseModule.forFeature([
            { name: MeasurementEvent.name, schema: MeasurementEventSchema }
        ])
    ],
    controllers: [MeasurementEventsController],
    providers: [MeasurementEventsService]
})
export class MeasurementEventsModule {}
