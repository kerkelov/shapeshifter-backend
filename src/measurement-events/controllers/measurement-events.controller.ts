import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
    UnauthorizedException
} from '@nestjs/common';
import { ObjectId } from 'mongoose';
import { AllowForRoles } from 'src/auth/decorators/allow-for-roles.decorator';
import { Role } from 'src/shared/enums/role';
import { CurrentUser } from '../../shared/decorators/current-user.decorator';
import { HasDate } from '../../shared/models/has-date.model';
import { HasIdentifier } from '../../shared/models/has-identifier.model';
import { HasStartAndEndDate } from '../../shared/models/has-start-and-end-date.model';
import { HasUser } from '../../shared/models/has-user.model';
import { wrapInHttpExceptionAndRethrow } from '../../shared/utils/error-helpers';
import { MeasurementEvent } from '../models/measurement-event.model';
import { MeasurementEventsService } from '../services/measurement-events.service';

@Controller('measurement-events')
@AllowForRoles(Role.User)
export class MeasurementEventsController {
    constructor(private measurementEventsService: MeasurementEventsService) {}

    @Post()
    @HttpCode(HttpStatus.CREATED)
    create(
        @Body() measurementEvent: MeasurementEvent
    ): Promise<MeasurementEvent> {
        return this.measurementEventsService
            .create(measurementEvent)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get()
    @AllowForRoles(Role.Admin)
    getAll(): Promise<MeasurementEvent[]> {
        return this.measurementEventsService
            .getAll()
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get('byDate/:userId')
    getByDate(
        @Param() params: HasUser,
        @Query() query: HasDate,
        @CurrentUser('_id') currentUserId: ObjectId
    ): Promise<MeasurementEvent> {
        if (currentUserId.toString() !== params.userId) {
            throw new UnauthorizedException('Not allowed');
        }

        return this.measurementEventsService
            .getByDate(query.date, params.userId)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get('betweenDates/:userId')
    getBetweenDates(
        @Param() params: HasUser,
        @Query() query: HasStartAndEndDate,
        @CurrentUser('_id') currentUserId: ObjectId
    ): Promise<MeasurementEvent[]> {
        if (currentUserId.toString() !== params.userId) {
            throw new UnauthorizedException('Not allowed');
        }

        return this.measurementEventsService
            .getBetweenDates(query.startDate, query.endDate, params.userId)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get(':id')
    getById(@Param() params: HasIdentifier): Promise<MeasurementEvent> {
        return this.measurementEventsService
            .getById(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Patch(':id')
    update(
        @Param() params: HasIdentifier,
        @Body() partsToUpdate: Partial<MeasurementEvent>
    ): Promise<MeasurementEvent> {
        return this.measurementEventsService
            .update(params.id, partsToUpdate)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    delete(@Param() params: HasIdentifier): Promise<MeasurementEvent> {
        return this.measurementEventsService
            .delete(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }
}
