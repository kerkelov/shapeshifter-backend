import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Required } from 'src/shared/utils/mongoose-helpers';
import { User } from 'src/users/models/user.model';
import { RequiredWithType } from '../../shared/utils/mongoose-helpers';

@Schema()
export class MeasurementEventSchemaTemplate {
    @RequiredWithType([String])
    photosUrls: string[];

    @Required()
    weight: number;

    @RequiredWithType(mongoose.Schema.Types.ObjectId, { ref: User.name })
    user: string;

    @Required({ index: true })
    date: Date;

    @Prop()
    chest: number;
    @Prop()
    waist: number;
    @Prop()
    hips: number;
    @Prop()
    biceps: number;
}

export type MeasurementEventDocument = MeasurementEventSchemaTemplate &
    Document;

export const MeasurementEventSchema = SchemaFactory.createForClass(
    MeasurementEventSchemaTemplate
);
