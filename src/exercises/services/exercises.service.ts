import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import {
    ExerciseDocument,
    ExerciseSchemaTemplate
} from 'src/exercises/schemas/exercise.schema';
import { SUCH_IDENTIFIER_DOES_NOT_EXIST } from 'src/shared/constants/error-messages';
import { throwErrorIfNull } from 'src/shared/utils/error-helpers';
import { Exercise } from '../models/exercise.model';

@Injectable()
export class ExercisesService {
    constructor(
        @InjectModel(Exercise.name)
        private exerciseModel: Model<ExerciseDocument>
    ) {}

    async create(exercise: Exercise): Promise<Exercise> {
        const alreadyExists = await this.exerciseModel
            .findOne({
                name: exercise.name
            })
            .lean();

        if (alreadyExists) {
            throw new Error('Exercise type with such a name already exists');
        }

        return (await new this.exerciseModel(exercise).save()).toObject();
    }

    async getAll(
        primaryMuscles?: string[],
        secondaryMuscleGroups?: string[],
        flag?: 'and' | 'or'
    ): Promise<Exercise[]> {
        const filter: Record<string, any> = {};
        flag = flag ?? 'or';

        if (primaryMuscles) {
            filter.primaryMuscleGroup = { $in: primaryMuscles };
        } else if (secondaryMuscleGroups && flag === 'and') {
            filter.secondaryMuscleGroups = { $all: secondaryMuscleGroups };
        } else if (secondaryMuscleGroups && flag === 'or') {
            filter.secondaryMuscleGroups = { $in: secondaryMuscleGroups };
        }

        return await this.exerciseModel.find(filter).lean();
    }

    getById(id: string): Promise<Exercise> {
        return this.exerciseModel
            .findById(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    update(
        id: string,
        newExercise: Partial<ExerciseSchemaTemplate>
    ): Promise<Exercise> {
        return this.exerciseModel
            .findByIdAndUpdate(id, newExercise, {
                new: true
            })
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    delete(id: string): Promise<Exercise> {
        return this.exerciseModel
            .findByIdAndDelete(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }
}
