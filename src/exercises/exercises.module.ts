import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ExercisesController } from './controllers/exercises.controller';
import { ExerciseSchema } from './schemas/exercise.schema';
import { ExercisesService } from './services/exercises.service';
import { Exercise } from './models/exercise.model';

@Module({
    imports: [
        MongooseModule.forFeature([
            {
                name: Exercise.name,
                schema: ExerciseSchema
            }
        ])
    ],
    controllers: [ExercisesController],
    providers: [ExercisesService]
})
export class ExercisesModule {}
