import { IsBoolean, ValidateIf } from 'class-validator';
import { IsNameWithReasonableLength } from '../../shared/decorators/is-name-with-reasonable-length.decorator';
import { IsValidMuscleGroup } from '../../shared/decorators/is-valid-muscle-group.decorator';

export class Exercise {
    @IsNameWithReasonableLength()
    name: string;

    @ValidateIf((exercise) => !exercise.isCardio)
    @IsValidMuscleGroup()
    primaryMuscleGroup: string;

    @ValidateIf((exercise) => !exercise.isCardio)
    @IsValidMuscleGroup({ each: true })
    secondaryMuscleGroups: string[];

    @IsBoolean()
    isCardio: boolean;
}
