import { IsIn, IsOptional, ValidateIf } from 'class-validator';
import { IsValidMuscleGroup } from '../../shared/decorators/is-valid-muscle-group.decorator';

export class SearchByMuscleGroupsQuery {
    @IsOptional()
    @IsValidMuscleGroup({ each: true })
    primaryMuscleGroups: string[];

    @IsOptional()
    @ValidateIf((query) => !query.primaryMuscleGroup)
    @IsValidMuscleGroup({ each: true })
    secondaryMuscleGroups: string[];

    @IsOptional()
    @ValidateIf((query) => !query.primaryMuscleGroup)
    @IsIn(['and', 'or'])
    flag: 'and' | 'or';
}
