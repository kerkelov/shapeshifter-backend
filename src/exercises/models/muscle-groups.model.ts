export type MuscleGroup =
    | 'traps'
    | 'shoulders'
    | 'triceps'
    | 'biceps'
    | 'chest'
    | 'back'
    | 'forearms'
    | 'core'
    | 'calf'
    | 'legs'
    | 'neck';

export const validMuscleGroups: MuscleGroup[] = [
    'traps',
    'shoulders',
    'triceps',
    'biceps',
    'chest',
    'back',
    'forearms',
    'core',
    'calf',
    'legs',
    'neck'
];
