import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query
} from '@nestjs/common';
import { AllowForRoles } from 'src/auth/decorators/allow-for-roles.decorator';
import { Exercise } from 'src/exercises/models/exercise.model';
import { ExercisesService } from 'src/exercises/services/exercises.service';
import { Role } from 'src/shared/enums/role';
import { wrapInHttpExceptionAndRethrow } from 'src/shared/utils/error-helpers';
import { HasIdentifier } from '../../shared/models/has-identifier.model';
import { SearchByMuscleGroupsQuery } from '../models/search-by-muscle-groups-query.model';

@Controller('exercises')
@AllowForRoles(Role.Admin)
export class ExercisesController {
    constructor(private exercisesService: ExercisesService) {}

    @Post()
    @HttpCode(HttpStatus.CREATED)
    create(@Body() exercise: Exercise): Promise<Exercise> {
        return this.exercisesService
            .create(exercise)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get()
    @AllowForRoles(Role.User)
    getAll(
        @Query() queryParameters: SearchByMuscleGroupsQuery
    ): Promise<Exercise[]> {
        return this.exercisesService
            .getAll(
                queryParameters.primaryMuscleGroups,
                queryParameters.secondaryMuscleGroups,
                queryParameters.flag
            )
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get(':id')
    getById(@Param() params: HasIdentifier): Promise<Exercise> {
        return this.exercisesService
            .getById(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Patch(':id')
    update(
        @Param() params: HasIdentifier,
        @Body() partsToUpdate: Partial<Exercise>
    ): Promise<Exercise> {
        return this.exercisesService
            .update(params.id, partsToUpdate)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    delete(@Param() params: HasIdentifier): Promise<Exercise> {
        return this.exercisesService
            .delete(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }
}
