import { Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Required } from 'src/shared/utils/mongoose-helpers';
import { RequiredWithType } from '../../shared/utils/mongoose-helpers';
import { validMuscleGroups } from '../models/muscle-groups.model';

@Schema()
export class ExerciseSchemaTemplate {
    @Required()
    name: string;

    @RequiredWithType(String, { enum: validMuscleGroups })
    primaryMuscleGroup: string;

    @RequiredWithType([String], { enum: validMuscleGroups })
    secondaryMuscleGroups: string[];

    @Required()
    isCardio: boolean;
}

export type ExerciseDocument = ExerciseSchemaTemplate & Document;

export const ExerciseSchema = SchemaFactory.createForClass(
    ExerciseSchemaTemplate
);
