import { IsMongoId, IsOptional } from 'class-validator';
import { IsDateInReasonableRange } from 'src/shared/decorators/is-date-in-reasonable-range.decorator';
import { ExerciseExecution } from './exercise-execution.model';

export class WorkoutExecution {
    @IsOptional()
    @IsMongoId()
    workoutTemplate: string;

    @IsMongoId({ each: true })
    exerciseExecutions: ExerciseExecution[];

    @IsDateInReasonableRange()
    date: Date;

    @IsMongoId()
    user: string;
}
