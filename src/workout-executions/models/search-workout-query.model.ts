import { IsDateString, IsOptional, ValidateIf } from 'class-validator';
import { Exercise } from '../../exercises/models/exercise.model';
import { IsValidMuscleGroup } from '../../shared/decorators/is-valid-muscle-group.decorator';

export class SearchWorkoutQuery {
    @IsDateString()
    public startDate: Date;
    @IsDateString()
    public endDate: Date;

    @IsOptional()
    @IsValidMuscleGroup({ each: true })
    public musclesEngaged?: string[];

    @IsOptional()
    @ValidateIf((query) => !query.primaryMuscleGroup)
    public exercisesPerformed?: Exercise & { _id: string }[];
}
