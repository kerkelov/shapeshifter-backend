import { ObjectId } from 'mongoose';
import { Exercise } from '../../exercises/models/exercise.model';
import { WorkoutTemplate } from '../../workout-templates/models/workout-template.model';
import { Set } from './set.model';

type PopulatedExerciseExecution = {
    exercise: Exercise & { _id: ObjectId | string };
    sets: Set[];
};

export type PopulatedWorkoutExecution = {
    workoutTemplate: WorkoutTemplate;
    exerciseExecutions: PopulatedExerciseExecution[];
    date: Date;
    user: string;
};
