import { IsMongoId, ValidateNested } from 'class-validator';
import { Set } from './set.model';

export class ExerciseExecution {
    @IsMongoId()
    exercise: string;

    @ValidateNested()
    sets: Set[];
}
