import { IsOptional, Length } from 'class-validator';
import { IsNumberInRange } from 'src/shared/decorators/is-number-in-range.decorator';

export class Set {
    @IsOptional()
    @IsNumberInRange(-100, 500)
    kilograms: number;

    @IsOptional()
    @IsNumberInRange(0, 100)
    reps: number;

    @IsOptional()
    @IsNumberInRange(0, 18000) // 18_000 seconds = 5 hours
    time: number; //in seconds

    @IsOptional()
    @IsNumberInRange(0, 20000) // 20_000 meters = 20 kilometers
    distance: number; //in meters

    @IsOptional()
    @Length(3, 103)
    note: string;
}
