import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { WorkoutExecutionsController } from './controllers/workout-executions.controller';
import { WorkoutExecutionSchema } from './schemas/workout-execution.schema';
import { WorkoutExecutionsService } from './services/workout-executions.service';
import { WorkoutExecution } from './models/workout-execution.model';
import { ExerciseExecutionsController } from './controllers/exercise-executions.controller';
import { ExerciseExecutionsService } from './services/exercise-executions.service';
import { ExerciseExecution } from './models/exercise-execution.model';
import { ExerciseExecutionSchema } from './schemas/exercise-execution.schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: WorkoutExecution.name, schema: WorkoutExecutionSchema },
            { name: ExerciseExecution.name, schema: ExerciseExecutionSchema }
        ])
    ],
    controllers: [WorkoutExecutionsController, ExerciseExecutionsController],
    providers: [WorkoutExecutionsService, ExerciseExecutionsService]
})
export class WorkoutExecutionsModule {}
