import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Post,
    Put
} from '@nestjs/common';
import { AllowForRoles } from 'src/auth/decorators/allow-for-roles.decorator';
import { Role } from 'src/shared/enums/role';
import { wrapInHttpExceptionAndRethrow } from 'src/shared/utils/error-helpers';
import { HasIdentifier } from '../../shared/models/has-identifier.model';
import { ExerciseExecution } from '../models/exercise-execution.model';
import { ExerciseExecutionsService } from '../services/exercise-executions.service';

@Controller('exercise-executions')
@AllowForRoles(Role.User)
export class ExerciseExecutionsController {
    constructor(private exerciseExecutionsService: ExerciseExecutionsService) {}

    @Post()
    @HttpCode(HttpStatus.CREATED)
    create(
        @Body() exerciseExecution: ExerciseExecution
    ): Promise<ExerciseExecution> {
        return this.exerciseExecutionsService
            .create(exerciseExecution)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get()
    getAll(): Promise<ExerciseExecution[]> {
        return this.exerciseExecutionsService
            .getAll()
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get(':id')
    getById(@Param() params: HasIdentifier): Promise<ExerciseExecution> {
        return this.exerciseExecutionsService
            .getById(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Put(':id')
    update(
        @Param() params: HasIdentifier,
        @Body() partsToUpdate: Partial<ExerciseExecution>
    ): Promise<ExerciseExecution> {
        return this.exerciseExecutionsService
            .update(params.id, partsToUpdate)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    delete(@Param() params: HasIdentifier): Promise<ExerciseExecution> {
        return this.exerciseExecutionsService
            .delete(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }
}
