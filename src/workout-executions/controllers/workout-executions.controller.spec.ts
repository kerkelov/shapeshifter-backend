import { Test, TestingModule } from '@nestjs/testing';
import { WorkoutExecutionsController } from './workout-executions.controller';

describe('WorkoutExecutionsController', () => {
    let controller: WorkoutExecutionsController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [WorkoutExecutionsController]
        }).compile();

        controller = module.get<WorkoutExecutionsController>(
            WorkoutExecutionsController
        );
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
