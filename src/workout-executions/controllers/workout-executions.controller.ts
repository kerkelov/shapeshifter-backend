import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Patch,
    Post,
    Query,
    UnauthorizedException
} from '@nestjs/common';
import { ObjectId } from 'mongoose';
import { AllowForRoles } from 'src/auth/decorators/allow-for-roles.decorator';
import { Role } from 'src/shared/enums/role';
import { wrapInHttpExceptionAndRethrow } from 'src/shared/utils/error-helpers';
import { CurrentUser } from '../../shared/decorators/current-user.decorator';
import { HasDate } from '../../shared/models/has-date.model';
import { HasIdentifier } from '../../shared/models/has-identifier.model';
import { HasStartAndEndDate } from '../../shared/models/has-start-and-end-date.model';
import { HasUser } from '../../shared/models/has-user.model';
import { PopulatedWorkoutExecution } from '../models/populated-workout-execution.model';
import { SearchWorkoutQuery } from '../models/search-workout-query.model';
import { WorkoutExecution } from '../models/workout-execution.model';
import { WorkoutExecutionsService } from '../services/workout-executions.service';

@Controller('workout-executions')
@AllowForRoles(Role.User)
export class WorkoutExecutionsController {
    constructor(private workoutExecutionsService: WorkoutExecutionsService) {}

    @Post()
    @HttpCode(HttpStatus.CREATED)
    create(
        @Body() workoutExecution: WorkoutExecution
    ): Promise<WorkoutExecution> {
        return this.workoutExecutionsService
            .create(workoutExecution)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get()
    @AllowForRoles(Role.Admin)
    getAll(): Promise<WorkoutExecution[]> {
        return this.workoutExecutionsService
            .getAll()
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get('byDate/:userId')
    getByDate(
        @Param() params: HasUser,
        @Query() query: HasDate,
        @CurrentUser('_id') currentUserId: ObjectId
    ): Promise<WorkoutExecution[]> {
        if (currentUserId.toString() !== params.userId) {
            throw new UnauthorizedException('Not allowed');
        }

        return this.workoutExecutionsService
            .getByDate(query.date, params.userId)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get('betweenDates/:userId')
    async getBetweenDates(
        @Param() params: HasUser,
        @Query() query: HasStartAndEndDate,
        @CurrentUser('_id') currentUserId: ObjectId
    ): Promise<[string, WorkoutExecution[]][]> {
        if (currentUserId.toString() !== params.userId) {
            throw new UnauthorizedException('Not allowed');
        }
        const resultsMap = await this.workoutExecutionsService
            .getBetweenDates(query.startDate, query.endDate, params.userId)
            .catch(wrapInHttpExceptionAndRethrow);

        return [...resultsMap.entries()];
    }

    @Post('search/:userId')
    @HttpCode(HttpStatus.OK)
    async search(
        @Param() params: HasUser,
        @Body() body: SearchWorkoutQuery,
        @CurrentUser('_id') currentUserId: ObjectId
    ): Promise<PopulatedWorkoutExecution[]> {
        if (currentUserId.toString() !== params.userId) {
            throw new UnauthorizedException('Not allowed');
        }

        return await this.workoutExecutionsService
            .search(
                body.startDate,
                body.endDate,
                params.userId,
                body.musclesEngaged,
                body.exercisesPerformed
            )
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get(':id')
    getById(@Param() params: HasIdentifier): Promise<WorkoutExecution> {
        return this.workoutExecutionsService
            .getById(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Patch(':id')
    update(
        @Param() params: HasIdentifier,
        @Body() partsToUpdate: Partial<WorkoutExecution>
    ): Promise<WorkoutExecution> {
        return this.workoutExecutionsService
            .update(params.id, partsToUpdate)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    delete(@Param() params: HasIdentifier): Promise<WorkoutExecution> {
        return this.workoutExecutionsService
            .delete(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }
}
