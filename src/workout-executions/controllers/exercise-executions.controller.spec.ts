import { Test, TestingModule } from '@nestjs/testing';
import { ExerciseExecutionsController } from './workout-executions.controller';

describe('ExerciseExecutionsController', () => {
    let controller: ExerciseExecutionsController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [ExerciseExecutionsController]
        }).compile();

        controller = module.get<ExerciseExecutionsController>(
            ExerciseExecutionsController
        );
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
