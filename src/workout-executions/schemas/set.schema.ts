import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';

@Schema()
export class SetSchemaTemplate {
    @Prop()
    kilograms: number;

    @Prop()
    reps: number;

    @Prop()
    time: number; //in seconds

    @Prop()
    distance: number; //in meters

    @Prop()
    note: string;
}

export const SetSchema = SchemaFactory.createForClass(SetSchemaTemplate);
