import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';

import { Required, RequiredWithType } from 'src/shared/utils/mongoose-helpers';
import { User } from 'src/users/models/user.model';
import { WorkoutTemplate } from 'src/workout-templates/models/workout-template.model';
import { ExerciseExecution } from '../models/exercise-execution.model';

@Schema()
export class WorkoutExecutionSchemaTemplate {
    @RequiredWithType([
        { type: mongoose.Types.ObjectId, ref: ExerciseExecution.name }
    ])
    exerciseExecutions: ExerciseExecution[];

    @RequiredWithType(mongoose.Types.ObjectId, { ref: User.name })
    user: string;

    @Prop({ type: mongoose.Types.ObjectId, ref: WorkoutTemplate.name })
    workoutTemplate: string;

    @Required()
    date: Date;
}

export type WorkoutExecutionDocument = WorkoutExecutionSchemaTemplate &
    Document;

export const WorkoutExecutionSchema = SchemaFactory.createForClass(
    WorkoutExecutionSchemaTemplate
);
