import { Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Exercise } from 'src/exercises/models/exercise.model';
import { RequiredWithType } from 'src/shared/utils/mongoose-helpers';
import { Set } from '../models/set.model';
import { SetSchema } from './set.schema';

@Schema()
export class ExerciseExecutionSchemaTemplate {
    @RequiredWithType(mongoose.Types.ObjectId, { ref: Exercise.name })
    exercise: string;

    @RequiredWithType([SetSchema])
    sets: Set[];
}

export type ExerciseExecutionDocument = ExerciseExecutionSchemaTemplate &
    Document;

export const ExerciseExecutionSchema = SchemaFactory.createForClass(
    ExerciseExecutionSchemaTemplate
);
