import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SUCH_IDENTIFIER_DOES_NOT_EXIST } from 'src/shared/constants/error-messages';
import { throwErrorIfNull } from 'src/shared/utils/error-helpers';
import { ExerciseExecution } from '../models/exercise-execution.model';
import { ExerciseExecutionDocument } from '../schemas/exercise-execution.schema';

@Injectable()
export class ExerciseExecutionsService {
    constructor(
        @InjectModel(ExerciseExecution.name)
        private exerciseExecutionModel: Model<ExerciseExecutionDocument>
    ) {}

    async create(
        exerciseExecution: ExerciseExecution
    ): Promise<ExerciseExecution> {
        if ((exerciseExecution as any)._id !== undefined) {
            delete (exerciseExecution as any)._id;
        }

        const allSetsAreContainingEnoughInformation =
            exerciseExecution.sets.length === 0 ||
            exerciseExecution.sets.every(
                (set) =>
                    (set.reps !== undefined && set.kilograms !== undefined) ||
                    (set.distance !== undefined && set.time !== undefined)
            );

        if (allSetsAreContainingEnoughInformation) {
            const createdExerciseExecution =
                await new this.exerciseExecutionModel(exerciseExecution).save();

            const createdExerciseExecutionWithPopulatedData =
                await this.exerciseExecutionModel
                    .findById(createdExerciseExecution._id)
                    .populate('exercise');

            return (
                createdExerciseExecutionWithPopulatedData ??
                createdExerciseExecution
            );
        }

        throw new Error(
            'sets should contain either reps and kilograms or time and distance'
        );
    }

    async getAll(): Promise<ExerciseExecution[]> {
        return await this.exerciseExecutionModel
            .find()
            .populate('exercise')
            .lean();
    }

    getById(id: string): Promise<ExerciseExecution> {
        return this.exerciseExecutionModel
            .findById(id)
            .populate('exercise')
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    update(
        id: string,
        partsToUpdate: Partial<ExerciseExecution>
    ): Promise<ExerciseExecution> {
        return this.exerciseExecutionModel
            .findByIdAndUpdate(id, partsToUpdate, {
                new: true
            })
            .populate('exercise')
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    delete(id: string): Promise<ExerciseExecution> {
        return this.exerciseExecutionModel
            .findByIdAndDelete(id)
            .populate('exercise')
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }
}
