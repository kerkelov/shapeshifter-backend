import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SUCH_IDENTIFIER_DOES_NOT_EXIST } from 'src/shared/constants/error-messages';
import { throwErrorIfNull } from 'src/shared/utils/error-helpers';
import { Exercise } from '../../exercises/models/exercise.model';
import { formatDate } from '../../shared/utils/format-date';
import { PopulatedWorkoutExecution } from '../models/populated-workout-execution.model';
import { WorkoutExecution } from '../models/workout-execution.model';
import { WorkoutExecutionDocument } from '../schemas/workout-execution.schema';

@Injectable()
export class WorkoutExecutionsService {
    constructor(
        @InjectModel(WorkoutExecution.name)
        private workoutExecutionModel: Model<WorkoutExecutionDocument>
    ) {}

    async create(
        workoutExecution: WorkoutExecution
    ): Promise<WorkoutExecution> {
        const createdWorkoutExecution = await new this.workoutExecutionModel(
            workoutExecution
        ).save();

        const workoutWithPopulatedData = (
            await createdWorkoutExecution.populate('workoutTemplate')
        ).populate({
            path: 'exerciseExecutions',
            populate: { path: 'exercise' }
        });

        return workoutWithPopulatedData;
    }

    async getAll(): Promise<WorkoutExecution[]> {
        return await this.workoutExecutionModel
            .find()
            .populate({
                path: 'exerciseExecutions',
                populate: { path: 'exercise' }
            })
            .populate('workoutTemplate')
            .lean();
    }

    getById(id: string): Promise<WorkoutExecution> {
        return this.workoutExecutionModel
            .findById(id)
            .populate({
                path: 'exerciseExecutions',
                populate: { path: 'exercise' }
            })
            .populate('workoutTemplate')
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    getByDate(date: Date, userId: string): Promise<WorkoutExecution[]> {
        const withTheSameDateAndUser = {
            date,
            user: userId
        };

        return this.workoutExecutionModel
            .find(withTheSameDateAndUser)
            .populate({
                path: 'exerciseExecutions',
                populate: { path: 'exercise' }
            })
            .populate('workoutTemplate')
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    async getBetweenDates(
        startDate: Date,
        endDate: Date,
        userId: string
    ): Promise<Map<string, WorkoutExecution[]>> {
        const withDateInTheGivenRangeAndTheSameUser = {
            date: { $gte: startDate, $lte: endDate },
            user: userId
        };

        const searchResults: WorkoutExecution[] =
            await this.workoutExecutionModel
                .find(withDateInTheGivenRangeAndTheSameUser)
                .populate({
                    path: 'exerciseExecutions',
                    populate: { path: 'exercise' }
                })
                .lean();

        const workoutsForEachDayMap: Map<string, WorkoutExecution[]> =
            new Map();

        searchResults.forEach((workout) => {
            const workoutDate: string = formatDate(workout.date);

            if (!workoutsForEachDayMap.has(workoutDate)) {
                workoutsForEachDayMap.set(workoutDate, []);
            }

            workoutsForEachDayMap.get(workoutDate)?.push(workout);
        });

        return workoutsForEachDayMap;
    }

    async search(
        startDate: Date,
        endDate: Date,
        userId: string,
        musclesEngaged?: string[],
        exercisesPerformed?: Exercise & { _id: string }[]
    ): Promise<PopulatedWorkoutExecution[]> {
        const filter: Record<string, any> = {
                user: userId,
                date: { $gte: startDate, $lte: endDate }
            },
            allWorkoutsInThatPeriod: PopulatedWorkoutExecution[] =
                await this.workoutExecutionModel.find(filter).populate({
                    path: 'exerciseExecutions',
                    populate: { path: 'exercise' }
                });

        if (musclesEngaged) {
            const workoutsContainingAllMusclesEngaged =
                allWorkoutsInThatPeriod.filter((workout) => {
                    const set = new Set(
                        workout.exerciseExecutions.map(
                            (exerciseExecution) =>
                                exerciseExecution.exercise.primaryMuscleGroup
                        )
                    );

                    return musclesEngaged.every((muscle) => set.has(muscle));
                });

            return workoutsContainingAllMusclesEngaged;
        } else if (exercisesPerformed) {
            const workoutsContainingAllExercises =
                allWorkoutsInThatPeriod.filter((workout) => {
                    const set = new Set(
                        workout.exerciseExecutions.map((exerciseExecution) =>
                            exerciseExecution.exercise._id.toString()
                        )
                    );

                    return exercisesPerformed.every((exercise) =>
                        set.has(exercise._id.toString())
                    );
                });

            return workoutsContainingAllExercises;
        }

        return [];
    }

    update(
        id: string,
        partsToUpdate: Partial<WorkoutExecution>
    ): Promise<WorkoutExecution> {
        return this.workoutExecutionModel
            .findByIdAndUpdate(id, partsToUpdate, {
                new: true
            })
            .lean()
            .populate({
                path: 'exerciseExecutions',
                populate: { path: 'exercise' }
            })
            .populate('workoutTemplate')
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    delete(id: string): Promise<WorkoutExecution> {
        return this.workoutExecutionModel
            .findByIdAndDelete(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }
}
