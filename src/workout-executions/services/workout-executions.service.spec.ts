import { Test, TestingModule } from '@nestjs/testing';
import { WorkoutExecutionsService } from './workout-executions.service';

describe('WorkoutExecutionsService', () => {
    let service: WorkoutExecutionsService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [WorkoutExecutionsService]
        }).compile();

        service = module.get<WorkoutExecutionsService>(
            WorkoutExecutionsService
        );
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
