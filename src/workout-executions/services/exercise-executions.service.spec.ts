import { Test, TestingModule } from '@nestjs/testing';
import { ExerciseExecutionsService } from './exercise-executions.service';

describe('ExerciseExecutionsService', () => {
    let service: ExerciseExecutionsService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [ExerciseExecutionsService]
        }).compile();

        service = module.get<ExerciseExecutionsService>(
            ExerciseExecutionsService
        );
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
