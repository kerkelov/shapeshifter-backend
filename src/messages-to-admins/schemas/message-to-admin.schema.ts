import { Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { Required } from 'src/shared/utils/mongoose-helpers';

@Schema()
export class MessageToAdminSchemaTemplate {
    @Required()
    message: string;

    @Required()
    senderEmail: string;

    @Required()
    senderName: string;
}

export type MessageToAdminDocument = MessageToAdminSchemaTemplate & Document;

export const MessageToAdminSchema = SchemaFactory.createForClass(
    MessageToAdminSchemaTemplate
);
