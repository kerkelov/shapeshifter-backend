import { IsEmail, Length } from 'class-validator';
import { IsNameWithReasonableLength } from '../../shared/decorators/is-name-with-reasonable-length.decorator';

export class MessageToAdmin {
    @Length(5, 500)
    message: string;

    @IsEmail()
    @Length(8, 50)
    senderEmail: string;

    @IsNameWithReasonableLength()
    senderName: string;
}
