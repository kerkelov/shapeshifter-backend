import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MessagesToAdminsController } from './controllers/messages-to-admins.controller';
import { MessageToAdmin } from './models/message-to-admin.model';
import { MessageToAdminSchema } from './schemas/message-to-admin.schema';
import { MessagesToAdminsService } from './services/messages-to-admins.service';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name: MessageToAdmin.name, schema: MessageToAdminSchema }
        ])
    ],
    controllers: [MessagesToAdminsController],
    providers: [MessagesToAdminsService]
})
export class MessagesToAdminsModule {}
