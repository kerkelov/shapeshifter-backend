import { Test, TestingModule } from '@nestjs/testing';
import { MessagesToAdminsController } from './messages-to-admins.controller';

describe('MessagesToAdminsController', () => {
    let controller: MessagesToAdminsController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            controllers: [MessagesToAdminsController]
        }).compile();

        controller = module.get<MessagesToAdminsController>(
            MessagesToAdminsController
        );
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
