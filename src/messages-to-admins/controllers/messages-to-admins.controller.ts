import {
    Body,
    Controller,
    Delete,
    Get,
    HttpCode,
    HttpStatus,
    Param,
    Post
} from '@nestjs/common';
import { AllowForRoles } from 'src/auth/decorators/allow-for-roles.decorator';
import { wrapInHttpExceptionAndRethrow } from 'src/shared/utils/error-helpers';
import { Role } from '../../shared/enums/role';
import { HasIdentifier } from '../../shared/models/has-identifier.model';
import { MessageToAdmin } from '../models/message-to-admin.model';
import { MessagesToAdminsService } from '../services/messages-to-admins.service';

@Controller('messages-to-admins')
export class MessagesToAdminsController {
    constructor(private messagesToAdminsService: MessagesToAdminsService) {}

    @Post()
    @HttpCode(HttpStatus.CREATED)
    create(@Body() messageToAdmin: MessageToAdmin): Promise<MessageToAdmin> {
        return this.messagesToAdminsService
            .create(messageToAdmin)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get()
    @AllowForRoles(Role.Admin)
    getAll(): Promise<MessageToAdmin[]> {
        return this.messagesToAdminsService
            .getAll()
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Get(':id')
    @AllowForRoles(Role.Admin)
    getById(@Param() params: HasIdentifier): Promise<MessageToAdmin> {
        return this.messagesToAdminsService
            .getById(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }

    @Delete(':id')
    @HttpCode(HttpStatus.NO_CONTENT)
    @AllowForRoles(Role.Admin)
    delete(@Param() params: HasIdentifier): Promise<MessageToAdmin> {
        return this.messagesToAdminsService
            .delete(params.id)
            .catch(wrapInHttpExceptionAndRethrow);
    }
}
