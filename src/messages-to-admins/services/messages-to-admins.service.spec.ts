import { Test, TestingModule } from '@nestjs/testing';
import { MessagesToAdminsService } from './messages-to-admins.service';

describe('MessagesToAdminsService', () => {
    let service: MessagesToAdminsService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            providers: [MessagesToAdminsService]
        }).compile();

        service = module.get<MessagesToAdminsService>(MessagesToAdminsService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
