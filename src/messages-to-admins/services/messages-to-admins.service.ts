import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SUCH_IDENTIFIER_DOES_NOT_EXIST } from 'src/shared/constants/error-messages';
import { throwErrorIfNull } from '../../shared/utils/error-helpers';
import { MessageToAdmin } from '../models/message-to-admin.model';
import { MessageToAdminDocument } from '../schemas/message-to-admin.schema';

@Injectable()
export class MessagesToAdminsService {
    constructor(
        @InjectModel(MessageToAdmin.name)
        private messageToAdminModel: Model<MessageToAdminDocument>
    ) {}

    async create(messageToAdmin: MessageToAdmin): Promise<MessageToAdmin> {
        return (
            await new this.messageToAdminModel(messageToAdmin).save()
        ).toObject();
    }

    async getAll(): Promise<MessageToAdmin[]> {
        return await this.messageToAdminModel.find().lean();
    }

    getById(id: string): Promise<MessageToAdmin> {
        return this.messageToAdminModel
            .findById(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }

    delete(id: string): Promise<MessageToAdmin> {
        return this.messageToAdminModel
            .findByIdAndDelete(id)
            .lean()
            .then(throwErrorIfNull(SUCH_IDENTIFIER_DOES_NOT_EXIST));
    }
}
