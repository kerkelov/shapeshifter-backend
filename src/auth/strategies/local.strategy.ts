import { Strategy } from 'passport-local';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from '../services/auth.service';
import { User } from 'src/users/models/user.model';

//TODO: think about if this file needs tests
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private authService: AuthService) {
        super({ usernameField: 'email' });
    }

    async validate(email: string, password: string): Promise<User> {
        return await this.authService
            .validateUser(email, password)
            .then((user) => {
                if (!user) {
                    throw new UnauthorizedException('Invalid credentials');
                }

                return user;
            });
    }
}
