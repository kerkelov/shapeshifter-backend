import {
    Controller,
    Post,
    UseGuards,
    Req,
    BadRequestException,
    HttpCode,
    HttpStatus
} from '@nestjs/common';
import { User } from 'src/users/models/user.model';
import { LocalAuthGuard } from '../guards/localAuth.guard';

@Controller('auth')
export class AuthController {
    @Post('login')
    @UseGuards(LocalAuthGuard)
    @HttpCode(HttpStatus.OK)
    login(@Req() req: Request & { user: User }): User {
        return req.user;
    }

    @Post('logout')
    @HttpCode(HttpStatus.OK)
    logout(@Req() req: Request & { session: Record<string, any> }): {
        message: string;
    } {
        const wasAuthenticated = req.session.passport;
        req.session.destroy();

        if (wasAuthenticated) {
            return { message: 'Successfully logged out' };
        }

        throw new BadRequestException(
            'You cannot logout if you are not logged in'
        );
    }
}
