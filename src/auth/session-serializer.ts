import { PassportSerializer } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/services/users.service';

@Injectable()
export class SessionSerializer extends PassportSerializer {
    constructor(private usersService: UsersService) {
        super();
    }

    serializeUser(
        user: { _id: string },
        done: (err: Error | null, user: { id: string }) => void
    ) {
        return done(null, { id: user._id });
    }

    async deserializeUser(
        { id }: { id: string },
        done: (err: Error | null, payload: any) => void
    ) {
        return done(
            null,
            await this.usersService.getById(id).catch(() => null)
        );
    }
}
