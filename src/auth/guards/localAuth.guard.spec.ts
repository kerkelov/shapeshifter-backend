import { LocalAuthGuard } from './localAuth.guard';

describe('LocalAuthGuard', () => {
    it('should be defined', () => {
        expect(new LocalAuthGuard()).toBeDefined();
    });
});
