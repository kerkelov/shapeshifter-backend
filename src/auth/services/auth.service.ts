import { Injectable } from '@nestjs/common';
import { User } from 'src/users/models/user.model';
import { PasswordsService } from 'src/users/services/passwords.service';
import { wrapInHttpExceptionAndRethrow } from '../../shared/utils/error-helpers';
import { UsersService } from '../../users/services/users.service';

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService,
        private passwordsService: PasswordsService
    ) {}

    async validateUser(email: string, password: string): Promise<User | null> {
        const userFromDatabase = await this.usersService
            .getNotSerialized({
                email
            })
            .catch(wrapInHttpExceptionAndRethrow);

        if (
            userFromDatabase &&
            (await this.passwordsService.compare(
                password,
                userFromDatabase.password
            ))
        ) {
            const userWithoutPassword = this.usersService.getByEmail(
                userFromDatabase.email
            );

            return userWithoutPassword;
        }

        return null;
    }
}
