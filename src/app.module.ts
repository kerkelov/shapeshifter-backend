import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './auth/auth.module';
import { ExercisesModule } from './exercises/exercises.module';
import { MeasurementEventsModule } from './measurement-events/measurement-events.module';
import { MessagesToAdminsModule } from './messages-to-admins/messages-to-admins.module';
import { SectionsModule } from './sections/sections.module';
import { UsersModule } from './users/users.module';
import { WorkoutExecutionsModule } from './workout-executions/workout-executions.module';
import { WorkoutTemplatesModule } from './workout-templates/workout-templates.module';

@Module({
    imports: [
        AuthModule,
        UsersModule,
        ExercisesModule,
        MeasurementEventsModule,
        SectionsModule,
        WorkoutExecutionsModule,
        WorkoutTemplatesModule,
        MessagesToAdminsModule,
        ConfigModule.forRoot({
            isGlobal: true,
            envFilePath: '.env'
        }),
        MongooseModule.forRoot(
            process.env.CONNECTION_STRING || 'mongodb://localhost/shapeshifter'
        )
    ]
})
export class AppModule {}
